import React from 'react';
import logo from './logo.svg';
import './App.css';

import {Calculator} from './components/Calculator';

function App() {
	return (
		<div className="App">
			<header className="App-header">
				<img src={logo} className="App-logo" alt="logo" />
					
				<Calculator />
				
				<a
					className="App-link"
					href="https://gitlab.com/learn-tech-projects/reactjs-practice-projects/react-calculator"
					target="_blank"
					rel="noopener noreferrer"
				>
					Project on github
				</a>

				<a
					className="App-link"
					href="https://reactjs.org"
					target="_blank"
					rel="noopener noreferrer"
				>
					Learn React
				</a>				
			</header>
		</div>
	);
}

export default App;
