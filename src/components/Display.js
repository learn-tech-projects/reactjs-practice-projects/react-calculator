import React, { Component } from 'react';

import './Display.scss'

export class Display extends Component {
	

	constructor(props){
		super(props)

		this.state = {
			
		}
	}


	render() {
		let displayStyle={
			backgroundColor:this.props.backgroundColor,
			color:this.props.color,
		}

		return (
			<div style={displayStyle} className="display">
				{this.props.display}
			</div>
		);
	}
}
