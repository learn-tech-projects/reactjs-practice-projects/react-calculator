import React, { Component } from 'react';

import {Button} from './Button';
import {Display} from './Display';

import './Calculator.scss'

export class Calculator extends Component {
	constructor(props){
		super(props)
		
		this.state = {
			value:"0",
			a:"",
			b:"",
			buff:"",
			command:""
		}

		this.handleClick = this.handleClick.bind(this);
	}

	handleClick(value ,e ){
		switch(value){
			case 'clear':
				this.clear();
				this.setState({
					a:"",
					b:"",
					command:""
				})
				break;
			case '+':
			case '-':
			case '*':
			case '/':
				this.setState((prevState)=>({
					command:value,
					a:prevState.value,
					value:"0"
				}))
				break;
			case '=':
				let {command,a} = this.state
				
				let b = parseInt(this.state.value)
				a = parseInt(a)

				let result = '';
				
				switch(command){
					case "+":
						result = this.sum(a,b)
						break;
					case "-":
						result = this.minus(a,b)
						break;
					case "*":
						result = this.multiply(a,b)
						break;
					case "/":
						result = this.divide(a,b)
						break;
					default:
						break;
				}

				this.setState((prevState)=>({
					a:"",
					b:"",
					command:"",
					value:result
				}))
				break;
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				if(this.state.value==="0"){
					this.setState({value:value})
				} else{
					this.setState((prevState)=>({
						value:prevState.value+value
					}))
				}
				break;
			case '0':
				if(this.state.value!=="0"){
					this.setState( (prevState)=>({
						value:prevState.value+value
					}) )
				}
				break;
			default:
				break;
		}

	}

	clear = () => this.setState({value:"0"});

	sum = (a,b) => a+b;

	minus = (a,b) => {
		return a-b;
	}
	
	multiply = (a,b) => a*b;

	divide = (a,b) => parseFloat(a/b).toFixed(2);


	render() {
		return (
			<div className="calculator">
				
				<div className="calculator__display">
					<Display backgroundColor="#433437" color="#fff" display={this.state.value}/>
				</div>

				<div className="calculator__buttons">
					<Button clickHandler={this.handleClick} text="clear" gridColumn="3" backgroundColor="#fff" color="#555" border="#292929"/>
					<Button clickHandler={this.handleClick} text="*" backgroundColor="#f2274f" color="#fff" border="#292929"/>
				</div>

				<div className="calculator__buttons">
					<Button clickHandler={this.handleClick} text="0" gridColumn="3" backgroundColor="#fff" color="#555" border="#292929"/>
					<Button clickHandler={this.handleClick} text="/" backgroundColor="#f2274f" color="#fff" border="#292929"/>
				</div>

				<div className="calculator__buttons">
					<Button clickHandler={this.handleClick} text="7" backgroundColor="#fff" color="#292929" border="#292929"/>
					<Button clickHandler={this.handleClick} text="8" backgroundColor="#fff" color="#292929" border="#292929"/>
					<Button clickHandler={this.handleClick} text="9" backgroundColor="#fff" color="#292929" border="#292929"/>
					<Button clickHandler={this.handleClick} text="-" backgroundColor="#f2274f" color="#fff" border="#292929"/>
				</div>

				<div className="calculator__buttons">
					<Button clickHandler={this.handleClick} text="4" backgroundColor="#fff" color="#292929" border="#292929"/>
					<Button clickHandler={this.handleClick} text="5" backgroundColor="#fff" color="#292929" border="#292929"/>
					<Button clickHandler={this.handleClick} text="6" backgroundColor="#fff" color="#292929" border="#292929"/>
					<Button clickHandler={this.handleClick} text="+" backgroundColor="#f2274f" color="#fff" border="#292929"/>
				</div>

				<div className="calculator__buttons">
					<Button clickHandler={this.handleClick} text="1" backgroundColor="#fff" color="#292929" border="#292929"/>
					<Button clickHandler={this.handleClick} text="2" backgroundColor="#fff" color="#292929" border="#292929"/>
					<Button clickHandler={this.handleClick} text="3" backgroundColor="#fff" color="#292929" border="#292929"/>
					<Button clickHandler={this.handleClick} text="=" backgroundColor="#f2274f" color="#fff" border="#292929"/>
				</div>



			</div>
		);
	}
}
