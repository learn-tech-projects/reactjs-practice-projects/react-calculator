import React, { Component } from 'react';

import './Button.scss'

export class Button extends Component {
	

	constructor(props){
		super(props)

		this.state = {
			text : this.props.text,
		}
	}

	render() {
		let buttonStyle = {
			color:this.props.color,
			backgroundColor:this.props.backgroundColor,
			gridColumn:"span "+this.props.gridColumn,
			border:"1px solid "+this.props.border,
		}

		return (
			<div onClick={(e) => this.props.clickHandler(this.state.text, e) } className="button" style={buttonStyle}>
				{this.state.text}
			</div>
		);
	}
}
